function renderDSSV(dssv) {
  var contentHTML = "";
  for (var i = dssv.length - 1; i >= 0; i--) {
    var item = dssv[i];
    var contentTr = `
    <tr>
        <td> ${item.ma} </td>
        <td> ${item.ten} </td>
        <td> ${item.email} </td>
        <td> ${item.tinhDTB()} </td>
        <td>
             <button 
             onclick="xoaSV('${item.ma}')"
             class="btn btn-danger">
             Xoá
             </button>
               <button 
             onclick="suaSV('${item.ma}')"
             class="btn btn-warning">
             Sửa
             </button>
        </td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  // lấy thông tin từ form
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value;
  var ly = document.getElementById("txtDiemLy").value;
  var hoa = document.getElementById("txtDiemHoa").value;

  // return {
  //   ma: ma,
  //   ten: ten,
  //   email: email,
  //   matKhau: matKhau,
  //   toan: toan,
  //   ly: ly,
  //   hoa: hoa,
  // };
  return new SinhVien(ma, ten, email, matKhau, toan, ly, hoa);
}
