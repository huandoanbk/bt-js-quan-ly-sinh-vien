// thêm sv
var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL";
// Load dữ liệu từ localStorage

var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  // JSON.parse(jsonData) => array
  // convert array cũ ( lấy localStorage ) => không có key tinhDTB() => khi lưu xuống bị mất => khi lấy lên ko còn => convert thành array mới
  dssv = JSON.parse(jsonData).map(function (item) {
    // item : phần tử của array trong các lần lặp
    // return của map()
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });
  renderDSSV(dssv);
  // map js
}

function themSV() {
  var sv = layThongTinTuForm();
  //   push(): thêm phần tử vào array

  dssv.push(sv);
  // convert data
  let dataJson = JSON.stringify(dssv);
  // lưu vào localStorage
  localStorage.setItem(DSSV_LOCAL, dataJson);
  //   render dssv lên table
  renderDSSV(dssv);
  //   tbodySinhVien
  resetForm();
}

function xoaSV(id) {
  // splice: cut  vs slice: copy
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    // nếu tìm thấy vị trí thì xoá
    // splice ( vị trí, số lượng)
    dssv.splice(viTri, 1);
    renderDSSV(dssv);
  }
}

function suaSV(id) {
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  // show thông tin lên form
  var sv = dssv[viTri];
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function capNhatSV() {
  //  layThongTinTuForm() => return object sv
  var sv = layThongTinTuForm();
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  dssv[viTri] = sv;
  renderDSSV(dssv);
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
